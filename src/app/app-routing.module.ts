import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { EmployeeDetailsComponent } from './employee-details/employee-details.component';
import { EmployeePayslipComponent } from './employee-payslip/employee-payslip.component';

const routes: Routes = [
  { path: '', redirectTo: 'details', pathMatch: 'full' },
  { path: 'details', component: EmployeeDetailsComponent },
  { path: 'payslip', component: EmployeePayslipComponent },
  { path: '**', redirectTo: 'details', pathMatch: 'full' }];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})


export class AppRoutingModule { }
