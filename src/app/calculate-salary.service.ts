import { Injectable } from '@angular/core';
import { Employee } from './employee';
import { Payslip } from './payslip';

@Injectable()
export class CalculateSalaryService {
  details = new Employee("","");
  slip = new Payslip();

  saveDetails(model)
  {
    this.details = model;
    if(this.details.annualSalary === undefined) 
      this.details.annualSalary = 0;
    if(this.details.superRate === undefined) 
      this.details.superRate = 0;
  }
  performCalculation()
  {
    var startDate = new Date(this.details.paymentStartDate);
    var monthDays = this.getPayPeriod(startDate.getFullYear(), startDate.getMonth());    
    var toDate = new Date( startDate.getFullYear(), startDate.getMonth(), monthDays);
    var perDayGrossIncome = (this.details.annualSalary/12)/monthDays;

    this.slip.name = this.details.firstName + " "+ this.details.lastName;
    this.slip.payPeriod = monthDays - startDate.getDate();
    this.slip.grossIncome = Math.round(perDayGrossIncome * this.slip.payPeriod);
    this.slip.incomeTax = this.applicableTax(this.slip.grossIncome);
    this.slip.netIncome = this.slip.grossIncome - this.slip.incomeTax;
    this.slip.superAnnuation = Math.round(this.slip.grossIncome * ((100+this.details.superRate)/100));
    this.slip.date = startDate.toDateString() + " - "+ toDate.toDateString();
  }
  getPayslip()
  {
    this.performCalculation();
    return this.slip;    
  }  
  applicableTax(income)
  {
    if(18200 < income && income <= 37000)
      return Math.round((income - 18200) * 0.19);
    else if(37000 < income && income <= 87000)
      return Math.round(3572 + ((income - 37000) * 0.325));
    else if(87000 < income && income <= 180000)
      return Math.round(19822 + ((income - 87000) * 0.37));
    else if(18000 < income)
      return Math.round(54232 + ((income - 180000) * 0.45));
    else 
      return 0;
  }
  getPayPeriod(year, month)
  {
    switch(month) {
      case 1:
        if((year%4) == 0)
          return 29;
        else
          return 28;
      case 0:
      case 2:
      case 4:
      case 6:
      case 7:    
      case 9:
      case 11:
        return 31;
      case 3:
      case 5:
      case 8:
      case 10:
        return 30;
    }
  }
}
