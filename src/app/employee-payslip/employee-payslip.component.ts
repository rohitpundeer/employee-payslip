import { Component, OnInit } from '@angular/core';
import { CalculateSalaryService } from '../calculate-salary.service';
import { Payslip }    from '../payslip';
import { Router } from '@angular/router';

@Component({
  selector: 'app-employee-payslip',
  templateUrl: './employee-payslip.component.html',
  styleUrls: ['./employee-payslip.component.css']
})
export class EmployeePayslipComponent implements OnInit {
  slip = new Payslip();
  constructor(private calculateSalaryService : CalculateSalaryService, private router : Router) { }

  ngOnInit() {
    this.slip = this.calculateSalaryService.getPayslip();
    if(this.slip.name.trim() === "")
      this.router.navigate(['./details']);
  }

}
