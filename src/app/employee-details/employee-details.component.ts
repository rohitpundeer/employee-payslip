import { Component, Input, Output } from '@angular/core';
import { CalculateSalaryService } from '../calculate-salary.service';
import { Employee }    from '../employee';
import { Router } from '@angular/router';

@Component({
  selector: 'app-employee-details',
  templateUrl: './employee-details.component.html',
  styleUrls: ['./employee-details.component.css']
})
export class EmployeeDetailsComponent {
  model = new Employee("", "");
  constructor(private calculateSalaryService : CalculateSalaryService, private router : Router) { }

  calculatePayslip()
  { 
    this.calculateSalaryService.saveDetails(this.model);
    this.router.navigate(['./payslip']);
  }

  resetClicked()
  {
    this.model = new Employee("", "");
  }
  superRateChanged(e)  
  {
    if(this.model.superRate > 12)
      this.model.superRate = 12;
    else if(this.model.superRate < 0 || e.key == "-" || e.key =="+")
      this.model.superRate = 0;
  }
}
