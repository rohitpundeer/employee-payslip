export class Payslip {
    constructor(
      public name: string = "",
      public payPeriod : number = 0,
      public grossIncome : number = 0,
      public incomeTax : number = 0,
      public netIncome : number = 0,
      public superAnnuation : number = 0,
      public date : string = ""
    ) {  }  
  }