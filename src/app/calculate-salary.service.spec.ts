import { TestBed, inject } from '@angular/core/testing';

import { CalculateSalaryService } from './calculate-salary.service';

describe('CalculateSalaryService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CalculateSalaryService]
    });
  });

  it('should be created', inject([CalculateSalaryService], (service: CalculateSalaryService) => {
    expect(service).toBeTruthy();
  }));
});
