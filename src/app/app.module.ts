import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { EmployeeDetailsComponent } from './employee-details/employee-details.component';
import { EmployeePayslipComponent } from './employee-payslip/employee-payslip.component';
import { AppRoutingModule } from './/app-routing.module';
import { CalculateSalaryService } from './calculate-salary.service';


@NgModule({
  declarations: [
    AppComponent,
    EmployeeDetailsComponent,
    EmployeePayslipComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [CalculateSalaryService],
  bootstrap: [AppComponent]
})
export class AppModule { }
