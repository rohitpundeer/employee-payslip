export class Employee {
    constructor(
      public firstName: string,
      public lastName: string,
      public annualSalary?: number,
      public superRate?: number,
      public paymentStartDate?: Date
    ) {  }  
  }